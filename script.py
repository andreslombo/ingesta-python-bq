import pandas as pd
from google.cloud import bigquery
from google.oauth2 import service_account

project_id = 'ingt-gcp-001'
dataset_id = 'Drummond'
table_id = 'Samples'

credentials = service_account.Credentials.from_service_account_file(
    './service-account.json',
)

client = bigquery.Client(project=project_id, credentials=credentials)

csv_file = './PZ56.csv'
df_original = pd.read_csv(csv_file, delimiter=',', skipinitialspace=True)

df_original.columns = ['date', 'Level(m)']
for i in range(1,2):
    df_copy_i = df_original.copy()
    
    df_copy_i['variable'] = df_copy_i.columns[i]

    df_copy_i['value'] = df_copy_i.iloc[:,i].astype(float)
    df_copy_i = df_copy_i.drop(['Level(m)'], axis=1)
    df_copy_i['station'] = "PZ56"
    df_copy_i['source'] = 'manual'
    df_copy_i['state'] = 'validado por equipo de medición'
    df_copy_i = df_copy_i[['station', 'variable', 'value', 'state', 'date', 'source']]


    df_copy_i['date'] = pd.to_datetime(df_copy_i['date']).dt.strftime('%Y-%m-%d %H:%M:%S UTC')
    df_copy_i['value'] = df_copy_i['value'].astype(str)
    print(df_copy_i)
    
    job_config = bigquery.LoadJobConfig(
    autodetect=True, source_format=bigquery.SourceFormat.CSV,)
    job = client.load_table_from_dataframe(df_copy_i, f'{project_id}.{dataset_id}.{table_id}', job_config=job_config)

    job.result()
    
    print(f'Data from {csv_file} has been ingested into BigQuery table {table_id}.')


#print(df_original)

#df['date'] = pd.to_datetime(df['date'], format='%m/%d/%y %H:%M').dt.strftime('%Y-%m-%d %H:%M:%S UTC')





